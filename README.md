# jmux



## What is this?

A pair of bash scripts to be used on the RACC (or adapted for similar computing clusters) in order to access Jupyter Labs over remote network.

## Installation

Copy the `jmux` script somewhere onto RACC in your local directory.

Copy the `jhup` bash functions into your local machines `.bashrc` (or any other file which is sourced at login).

## Configure ssh

The scripts assume the presence of `cluster` in your ssh config (this is the remote machine to connect to). Either insert the relevant details into your ssh config using the name `cluster` (as in the template file `ssh_config`), or replace instances of `cluster` in the bash script with the appropriate name or url.

## Usage

Type `jhup-new` on your local device to initiate a new jupyter labs session on the remote host. You may be asked for ssh passwords.

After suitable time has passed (allow a few seconds for jupyter to get going), run `jhup-attach` to make the `ssh` bridge, retrieve the login url/code, and open the url in your web browser.

## Mac OS X support?

This software was written for linux, however, with minor modifications should also run on mac. The command `xdg-open` should be replaced with `open` in the `jhup-attach` function. Other modifications may be necessary - please raise a flag to have them included here.

## Contributing

Contributions welcome. Raise an issue or send me an email.

## License

CC4 share-alike
