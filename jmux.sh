#!/usr/bin/env bash

# PATHS CONFIGURATION
# path to jupyter
#  ensure jupyter is installed to 'mycustomenv' anaconda environment,
#  or point to other suitable location where jupyter is installed
JUPYTER=~/.conda/envs/mycustomenv/bin/jupyter
# directory to run jupyter
#  for example if your project files are in '~/important_research/folder/' then use that
NOTEBOOK_DIR=/storage/silver/scenario/ab123456
# directory to store cache
#  this can be anywhere with write access
#  we write jhup.out, a small log file to store node number, jupyter url, etc.
CACHE_FILE=/storage/silver/scenario/ab123456/jhup.out
# port number
#  can choose any number :) 
#  if the port is unavailble, will select PORT+1, PORT+2, ... etc.
JUPYTER_PORT=1234

# parse arguments
if [[ "${1}" == "new" ]]; then
    # get the login node
    echo ${HOSTNAME%.act.rdg.ac.uk} > $CACHE_FILE
    # run jupyter lab in background
    $JUPYTER lab --no-browser --port=${JUPYTER_PORT} ${NOTEBOOK_DIR} >> $CACHE_FILE 2>&1
elif [[ "${1}" == "list" ]]; then
    # check running sessions
    $JUPYTER server list
elif [[ "${1}" == "kill" ]]; then
    # close jupyter lab
    lsof -ti:$JUPYTER_PORT | xargs kill
else
    # no arg passed
    echo "Unknown argument '${1}'"
    exit 1
fi
