# define username
RACC_USER=ab123456
RACC_HOST=cluster
JHUP_CACHE_REMOTE=/path/to/cache/file/jhup.out
JHUP_CACHE_LOCAL=/tmp/jhup.out

# initiate new jupyter labs instance
# change this path to reflect the jmux installation path
jhup-new(){
	ssh -n -f cluster "sh -c 'nohup ~/Packages/jmux new > /dev/null 2>&1 &'";
}

# connect to most recently initiated jupyter labs instance
jhup-attach(){
	scp cluster:$JHUP_CACHE_REMOTE $JHUP_CACHE_LOCAL
	NODE=`cat $JHUP_CACHE_LOCAL | head -n1`
	URL=`cat $JHUP_CACHE_LOCAL | grep -A 2 "Or copy and paste one of these URLs" | head -n2 | tail -n1`
	PORT=${URL/*:/}
	PORT=${PORT/\/*/}
	ssh -o ProxyCommand="ssh -W %h:%p ${RACC_USER}@$RACC_HOST" $RACC_USER@$NODE -L localhost:$PORT:localhost:$PORT -N -f
	xdg-open $URL;
}
